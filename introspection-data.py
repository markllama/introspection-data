import json
import argparse
from prettytable import PrettyTable
import sys


class Introspect():
    def __init__(self, file):
        self.file = file
        try:
            with open(file) as f:
                self.data = json.load(f)
            self.generic = PrettyTable()
            self.generic.field_names = ["Key", "Value"]
            self.generic.align["Key"] = "l"
            self.generic.align["Value"] = "l"
            self.generic.add_row(["Manufacturer", self.data['inventory'].get('system_vendor').get('manufacturer')])
            self.generic.add_row(["Server Model", self.data['inventory'].get('system_vendor').get('product_name')])
            self.generic.add_row(["CPU Architecture", self.data['cpu_arch']])
            self.generic.add_row(["Memory(in MB)", self.data['inventory'].get('memory').get('physical_mb')])
            self.generic.add_row(["Root Disk size(in GB)", self.data['local_gb']])

        except FileNotFoundError as err:
            sys.exit("OS error: {0}".format(err))

        except ValueError:
            sys.exit("Invalid file: \"{0}\". File must be in JSON format".format(self.file))

    def port_mapping(self):
        if 'all_interfaces' in self.data.keys():
            self.port = PrettyTable()
            switch_location = {}
            for i in self.data['all_interfaces'].keys():
                self.port.field_names = ["Interface", "Mac Address", "Switch Location", "Switch Port", "has_carrier"]
                if 'lldp_processed' in self.data['all_interfaces'][i].keys():
                    self.port.add_row([i,
                               self.data['all_interfaces'][i]['mac'],
                               self.data['all_interfaces'][i]['lldp_processed']['switch_system_name'].upper(),
                               self.data['all_interfaces'][i]['lldp_processed']['switch_port_id']])

                    switch_location[i] = self.data['all_interfaces'][i]['lldp_processed']['switch_system_name']

                else:
                    self.port.add_row([i, self.data['all_interfaces'][i]['mac'], 'None', 'None'])
            return self.port
        else:
            sys.exit("Data not available")

    def disks(self):
        if 'disks' in self.data['inventory']:
            self.disk = PrettyTable()
            disk_count = len(self.data['inventory']['disks'])
            for i in range(disk_count):
                if self.data['inventory']['disks'][i].get('serial') == self.data['root_disk']['serial']:
                    root_disk = True
                else:
                    root_disk = False

                self.disk.field_names = ["Name", "Size(in bytes)", "Vendor", "Serial", "Path", "Rotational", "Model", "root_disk"]
                self.disk.add_row([self.data['inventory']['disks'][i].get('name'),
                                    self.data['inventory']['disks'][i].get('size'),
                                    self.data['inventory']['disks'][i].get('vendor'),
                                    self.data['inventory']['disks'][i].get('serial'),
                                    self.data['inventory']['disks'][i].get('by_path'),
                                    self.data['inventory']['disks'][i].get('rotational'),
                                    self.data['inventory']['disks'][i].get('model'),
                                   root_disk])
            return self.disk
        else:
            sys.exit("Data not available")


def introspection_data(args):
    introspect = Introspect(args.introspection_file)
    if args.port_mapping and args.disks:
        print(introspect.port_mapping())
        print(introspect.disks())
        sys.exit()
    if args.port_mapping:
        print(introspect.port_mapping())
        sys.exit()
    if args.disks:
        print(introspect.disks())
        sys.exit()
    print(introspect.generic)


def vlan_data(args):
    import nscli.auth.ns_auth
    import nscli.plugins.parsers
    from nscli.info.ns_info import InfoV2
    if args.device_id:
        token = nscli.auth.ns_auth.Token()
        parser = nscli.plugins.parsers.initialize_parser()
        v_args = parser.parse_args(["info", args.device_id, "-p"])
        vlan_data = PrettyTable()
        i = InfoV2(token, v_args)
        i.gather_list_info()
        i.port_status()
        device_info = i.computers_info()
        vlan_data.field_names = ['Switch Location', 'Switch Port', 'Allowed Vlans', 'Native Vlan', 'Speed', 'CDP']
        for switch_port in device_info.get(int(args.device_id)).get('port_data'):
            if 'vlans_enabled' not in switch_port['port_status']['trunking']:
                allowed_vlans = switch_port.get('port_status').get('vlan_id')
            else:
                allowed_vlans = switch_port.get('port_status').get('trunking').get('vlans_enabled')
            vlan_data.add_row(([switch_port['switch']+"."+switch_port['dc'],
                                switch_port.get('port_status').get('name'),
                                allowed_vlans,
                                switch_port.get('port_status').get('trunking').get('native_vlan'),
                                switch_port.get('port_status').get('speed'),
                                switch_port.get('port_status').get('cdp')]))
        print(vlan_data)


def main():
    parser = argparse.ArgumentParser(description="Fetch device and network information for the Red Hat Open Stack"
                                                 " deployment",
                                     epilog="This program will help you to fetch server and network information"
                                            " which is required for the Red Hat Open Stack deployment")
    # subparser main menu
    subparser = parser.add_subparsers(help="sub-command's",
                                      metavar="{introspect,vlan}")
    subparser.required = True

    # subparser for introspection data
    parser_introspect = subparser.add_parser('introspect',
                                             help='Analyze introspection file. Without any optional argument, the'
                                                  ' program will print basic server information.')
    parser_introspect.add_argument("introspection_file",
                                   help="Introspection file to analyze. It can be generated using"
                                        " `openstack baremetal introspection data save _UUID_`."
                                        " For more information, please refer Red Hat documentation.")
    parser_introspect.add_argument("--port-mapping",
                                   help="Show server interface mapping to the switch ports.",
                                   action="store_true",
                                   dest="port_mapping")
    parser_introspect.add_argument("--disks",
                                   help="Show available disks on the server",
                                   action="store_true",
                                   dest="disks")
    parser_introspect.set_defaults(func=introspection_data)

    # subparser for vlan data
    parser_vlan = subparser.add_parser('vlan',
                                       help='Fetch vlan information for the device from switchapi')
    parser_vlan.add_argument("device_id",
                             help='Device ID from the CORE (ex. "617153")')
    parser_vlan.set_defaults(func=vlan_data)
    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
