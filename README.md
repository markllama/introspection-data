# Introspection-data
Red Hat Open Stack installer(director/undercloud) performs deployment of production ready Open Stack(overcloud) using heat, puppet and ansible. Undercloud manages the installation of the operating system for overcloud nodes. As the operating system is not preinstalled on the overcloud nodes, its bit difficult the visualize the networking on the server. Hence, during the introspection process, undercloud gathers server information and upload it to the swift which can be fetched using `openstack` cli.

This program will help to fetch network and disk information of the server from introspection file of the server as well as network VLAN configuration from switch API. This information can be used later while planing networking of overcloud using heat templates.

# Prerequisite:
- Introspection information file of the server is downloaded from undercloud swift using `openstack` cli
  ```
  openstack baremetal introspection data save _UUID_ > 617167-ceph02.rpctnd1.iad3.com.json
  ```

# Installation:
```
$ git clone https://gitlab.com/pbandark/introspection-data.git
$ cd introspection-data
$ python3  -m venv virtual_env
$ source virtual_env/bin/activate
$ pip install -r requirement.txt
```

**Note:**
- Installation needs access to https://artifacts.rackspace.net/artifactory/api/pypi/pypi/simple/ 

# Usage:

```
$ python introspection-data.py -h
usage: introspection-data.py [-h] {introspect,vlan} ...

Fetch device and network information for the Red Hat Open Stack deployment

positional arguments:
  {introspect,vlan}  sub-command's
    introspect       Analyze introspection file. Without any optional
                     argument, the program will print basic server
                     information.
    vlan             Fetch vlan information for the device from switchapi

optional arguments:
  -h, --help         show this help message and exit

This program will help you to fetch server and network informationwhich is
required for the Red Hat Open Stack deployment
------------------------------------------

$ python introspection-data.py introspect -h
usage: abc introspect [-h] [--port-mapping] [--disks] introspection_file

positional arguments:
  introspection_file  Introspection file to analyze. It can be generated using
                      `openstack baremetal introspection data save _UUID_`.
                      For more information, please refer Red Hat
                      documentation.

optional arguments:
  -h, --help          show this help message and exit
  --port-mapping      Show server interface mapping to the switch ports.
  --disks             Show available disks on the server
------------------------------------------

$ python introspection-data.py vlan -h
usage: abc vlan [-h] device_id

positional arguments:
  device_id   Device ID from the CORE (ex. "617153")

optional arguments:
  -h, --help  show this help message and exit
```

# Example:
```
$ python3 introspection-data.py introspect 617167-ceph02.rpctnd1.iad3.com.json
+-----------------------+-----------------------------------------------------------+
| Key                   | Value                                                     |
+-----------------------+-----------------------------------------------------------+
| Manufacturer          | Dell Inc.                                                 |
| Server Model          | PowerEdge R720 (SKU=NotProvided;ModelName=PowerEdge R720) |
| CPU Architecture      | x86_64                                                    |
| Memory(in MB)         | 131072                                                    |
| Root Disk size(in GB) | 557                                                       |
+-----------------------+-----------------------------------------------------------+

$ python3 introspection-data.py introspect 617167-ceph02.rpctnd1.iad3.com.json  --port
+-----------+-------------------+-----------------+-------------+
| Interface |    Mac Address    | Switch Location | Switch Port |
+-----------+-------------------+-----------------+-------------+
|    p5p1   |        None       |       None      |     None    |
|    p5p3   |        None       |       None      |     None    |
|    p5p2   |        None       |       None      |     None    |
|    p5p4   |        None       |       None      |     None    |
|    p6p2   | 90:e2:ba:71:44:09 |   F2-1-2E.IAD3  |  Ethernet25 |
|    em4    |        None       |       None      |     None    |
|    p6p1   | 90:e2:ba:71:44:08 |   F2-1-2E.IAD3  |  Ethernet5  |
|    em1    | b8:ca:3a:6f:57:40 |   F2-1-1E.IAD3  |   Gi1/0/5   |
|    em3    |        None       |       None      |     None    |
|    em2    |        None       |       None      |     None    |
|    p7p2   | 90:e2:ba:71:3e:0d |   F2-2-3E.IAD3  |  Ethernet25 |
|    p7p1   | 90:e2:ba:71:3e:0c |   F2-2-3E.IAD3  |  Ethernet5  |
+-----------+-------------------+-----------------+-------------+

$ python3 introspection-data.py introspect 617167-ceph02.rpctnd1.iad3.com.json  --disk
+----------+----------------+--------+----------------------------------+-------------------------------------------------+------------+
|   Name   | Size(in bytes) | Vendor |              Serial              |                       Path                      | Rotational |
+----------+----------------+--------+----------------------------------+-------------------------------------------------+------------+
| /dev/sda |  599550590976  |  DELL  | 6c81f660f3446b001fa2d5a087417fc2 | /dev/disk/by-path/pci-0000:02:00.0-scsi-0:2:0:0 |    True    |
| /dev/sdb |  599550590976  |  DELL  | 6c81f660f3446b0020875f5d56e7602d | /dev/disk/by-path/pci-0000:02:00.0-scsi-0:2:1:0 |    True    |
| /dev/sdc |  599550590976  |  DELL  | 6c81f660f3446b00205558594588752e | /dev/disk/by-path/pci-0000:02:00.0-scsi-0:2:2:0 |    True    |
| /dev/sdd |  599550590976  |  DELL  | 6c81f660f3446b002055585d45c2e1aa | /dev/disk/by-path/pci-0000:02:00.0-scsi-0:2:3:0 |    True    |
| /dev/sde |  599550590976  |  DELL  | 6c81f660f3446b00224bf9bb1ccef9d5 | /dev/disk/by-path/pci-0000:02:00.0-scsi-0:2:5:0 |    True    |
| /dev/sdf |  599550590976  |  DELL  | 6c81f660f3446b0020e402bc500d3e99 | /dev/disk/by-path/pci-0000:02:00.0-scsi-0:2:6:0 |    True    |
+----------+----------------+--------+----------------------------------+-------------------------------------------------+------------+

$ python3 introspection-data.py vlan 616580
+-----------------+-------------+----------------+-------------+-------+
| Switch Location | Switch Port | Allowed Vlans  | Native Vlan | Speed |
+-----------------+-------------+----------------+-------------+-------+
|   F2-1-1E.IAD3  |   Gi1/0/1   |    574,575     |     575     |  1000 |
|   F2-1-2E.IAD3  |  Ethernet1  |    951,1125    |      1      | 10000 |
|   F2-1-2E.IAD3  |  Ethernet21 | 1124,1126-1132 |      1      | 10000 |
|   F2-2-3E.IAD3  |  Ethernet1  |    951,1125    |      1      | 10000 |
|   F2-2-3E.IAD3  |  Ethernet21 | 1124,1126-1132 |      1      | 10000 |
|   F2-1-1S.IAD3  |   Gi1/0/1   |      None      |     None    |  1000 |
|   F2-1-1E.IAD3  |   Gi1/0/21  |      None      |     None    |  100  |
+-----------------+-------------+----------------+-------------+-------+
```


**Known issue:**
- `python3 introspection-data.py vlan 616580` will fail with an error. 
  - Its bug[1] in `nspylib` library which is fixed in the latest code. But, the `nscli` will not get new library until the next release cycle.
  - As a workaround, add hex to binary mapping for alphabets A-F in `nspylib/util/vlans.py`.
  For ex:
  ```
  `virtual_env/lib/python3.7/site-packages/nspylib/util/vlans.py
  [...]
  70         hex_to_binary_dict = {
  [...]
  87             "A": "1010",
  88             "B": "1011",
  89             "C": "1100",
  90             "D": "1101",
  91             "E": "1110",
  92             "F": "1111",
  93         }
  ```
  [1] https://github.rackspace.com/NSI/nspylib/issues/137



